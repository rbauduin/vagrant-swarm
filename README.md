# About

This is a simple configuration to run a Docker Swarm with [Vagrant](https://www.vagrantup.com/) and [VirtualBox](https://www.virtualbox.org/). It starts a manager and 2 workers.
It is using Ubuntu 22.04 and installs docker according to the [instructions on the docker website](https://docs.docker.com/engine/install/ubuntu/).

# Usage

Clone this repo, get into the repo's directory, then start the vms with `vagrant up`.
Access manager with `vagrant ssh manager` or workers with `vagrant ssh worker1` and `vagrant ssh worker2`.

On the manager, you can validate the swarm is ready with the command `docker node ls`.
